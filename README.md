# Newspaper Comments

*This single-page project was built using the following tools:*

1. BEM methodology
2. Sass variables, mixins and animations
3. CSS grid
4. JavaScript:
    * getting elements by ID
    * adding an event listener
    * functions
    * test function