// Get the HTML elements that we need.
const form = document.getElementById("form");
const fullNameInput = document.getElementById("full-name");
const cityInput = document.getElementById("city");
const commentInput = document.getElementById("comment");
const button = document.getElementById("form-button");
const commentSection = document.getElementById("comment-section");

// Write the functions we need.
/**
 * Creates an object that contains the current
 * values of the input boxes.
 *
 * @returns {Object} The input values.
 */
function getFormValues() {
    const fullName = fullNameInput.value;
    const city = cityInput.value;
    const comment = commentInput.value;

    return {
        fullName,
        city,
        comment
    };
}

/**
 * Creates a new comment.
 * 
 * @param {string} fullName The user's full name.
 * @param {string} city The user's city.
 * @param {string} comment The user's comment.
 * 
 * @returns {HTMLDivElement} The comment.
 */
function createComment(fullName, city, comment) {
    // Create the elements we need.
    const commentDiv = document.createElement("div");
    const fullNameH4 = document.createElement("h4");
    const cityH6 = document.createElement("h6");
    const commentP = document.createElement("p");

    // Set their inner HTML.
    fullNameH4.innerHTML = fullName;
    cityH6.innerHTML = city;
    commentP.innerHTML = comment;

    // Add a class to each new element.
    commentDiv.classList.add("comment");
    fullNameH4.classList.add("comment__name");
    cityH6.classList.add("comment__city");
    commentP.classList.add("comment__text");

    // Build the profile.
    commentDiv.appendChild(fullNameH4);
    commentDiv.appendChild(cityH6);
    commentDiv.appendChild(commentP);

    return commentDiv;
}

/**
 * Responds to the button click.
 *
 * @param {Event} event The click event.
 *
 * @returns {void}
 */
 function formSubmitListener(event) {
    event.preventDefault(); // prevents the default behaviour of the form to be submitted.

    const values = getFormValues();

    const comment = createComment(
        values.fullName,
        values.city,
        values.comment
    );

    commentSection.appendChild(comment);

    form.reset();
}

// Tie everything together.
form.addEventListener("submit", formSubmitListener);

// function testComment() {
//     const comment = createComment("Petra", "Cardiff", "This is a comment");

//     if (comment === undefined) {
//         console.error("Failed to create a comment.");
//     } else {
//         console.log("Here is the comment...");
//         console.log(comment);
//     }
// }

// testComment();
